package sg.com.citi.tradefulfillmentservice.sim;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sg.com.citi.tradefulfillmentservice.dao.TradeSqlDao;
import sg.com.citi.tradefulfillmentservice.model.Trade;
import sg.com.citi.tradefulfillmentservice.model.TradeState;

import java.util.List;

@Component
@Slf4j
@EnableScheduling
public class TradeSim {

    @Autowired
    private TradeSqlDao tradeDao;

    @Transactional
    public List<Trade> findTradesForProcessing(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.CREATED);

        for(Trade thisTrade: foundTrades) {
            thisTrade.setState(TradeState.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.PROCESSING);

        for(Trade thisTrade: foundTrades) {
            if((int) (Math.random()*10) > 8) {
                thisTrade.setState(TradeState.REJECTED);
            }
            else {
                thisTrade.setState(TradeState.FILLED);
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

//    @Scheduled(fixedRate = 1000)
    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        log.info("Trade Fulfillment Service Scheduler Running!");

        int tradesForFilling = findTradesForFilling().size();
        log.info("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        log.info("Found " + tradesForProcessing + " trades to be processed");
    }
}
