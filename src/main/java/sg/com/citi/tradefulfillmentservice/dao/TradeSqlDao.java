package sg.com.citi.tradefulfillmentservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sg.com.citi.tradefulfillmentservice.model.Trade;
import sg.com.citi.tradefulfillmentservice.model.TradeState;

import java.util.List;

public interface TradeSqlDao extends JpaRepository<Trade, Integer> {

    List<Trade> findByState(TradeState state);
}
